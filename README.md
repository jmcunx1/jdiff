## jdiff - Sorted diff of Two Text Files

Will perform a diff on Two Sorted Text Files.

Most cases diff(1) is better, but if you need to compare
Two Very Large Extracts from say two different databases,
this will work fine.  But, the Files must be in sorted
order.

This works fine under Linux, BSD(s) and AIX.

You should use diff(1) if possible.  This was created to
compare very large files extracts from SQL Databases on
Systems that have a memory constrained diff(1).

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jdiff) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jdiff.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jdiff.gmi (mirror)

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
